#!/bin/bash


# ---------------------------------------------
# clean up and destroy password on script exit
# ---------------------------------------------
function cleanup() {
  rm -rf authdir/ /root/.docker/config.json
  export PACKTIVITY_AUTH_LOCATION=""
  export RECAST_REGISTRY_HOST=""
  export RECAST_REGISTRY_USERNAME=""
  export RECAST_REGISTRY_PASSWORD=""
  export YADAGE_SCHEMA_LOAD_TOKEN=""
  export RECAST_AUTH_USERNAME=""
  export RECAST_AUTH_PASSWORD=""
  export YADAGE_INIT_TOKEN=""
  eval $(recast auth destroy)
}
trap cleanup EXIT
trap cleanup SIGINT
trap cleanup SIGQUIT
trap cleanup SIGTSTP
# ---------------------------------------------

# on lxplus: setup RECAST
source ~recast/public/setup.sh || echo "Not on lxplus! Make sure that you installed yadage and recast-atlas."

# authentificate with credentials
if [ -z "$RECAST_USER" ]; then
  echo "Enter user name:"
  read RECAST_USER
fi
if [ -z "$RECAST_PASS" ]; then
  echo "Enter password:"
  read -s RECAST_PASS
fi
if [ -z "$RECAST_TOKEN" ]; then
  echo "Enter Gitlab API access token:"
  read RECAST_TOKEN
fi

eval "$(recast auth setup -a $RECAST_USER -a $RECAST_PASS -a $RECAST_TOKEN -a default)"
eval "$(recast auth write --basedir authdir)"

# add analysis temporarily to catalogue
$(recast catalogue add $PWD)
recast catalogue ls
recast catalogue describe monosww
recast catalogue check monosww

# run workflow
#rm -rf recast-myrun/
#recast run monosww --tag myrun
#recast tests run monosww test_selection_mc16a_local --backend docker --tag local_test
