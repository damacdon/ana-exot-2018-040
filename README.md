## RECAST setup for monoSVV Analysis (Hadronic Channel)

Code that runs the monoSVV analysis code + WSmaker statistical evaluation.

Analysis twiki: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MonoScalarWW
XAMPP event selection code: https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoS
WSmaker statistical evaluation code: https://gitlab.cern.ch/atlas-phys/exot/jdm/ana-exot-2018-40/WSMaker_MonoS

### setup for local workflow execution

To run the workflow locally, please follow these instructions:

```
pip install recast-atlas
source run.sh
```

You will be prompted to enter username, password and gitlab token.

When using Singularity instead of Docker (experimental) it is required to provide credentials for the gitlab registry with environment variables:
```
# Singularity
export SINGULARITY_DOCKER_USERNAME=...
export SINGULARITY_DOCKER_PASSWORD=...
```

